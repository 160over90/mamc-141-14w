	<?php get_header(); ?>
	<?php include 'index-header.php'; ?>
	<div id="hero-index" class="index">
		<img class="top-frame" src="<?php bloginfo('stylesheet_directory');?>/images/index-header.png" alt="header" />
		<div class="row">
			<div class="col-sm-12">
				<a href="/"><img class="logo" src="<?php bloginfo('stylesheet_directory');?>/images/header-logo.svg" alt="logo" /></a>
			</div>
		</div>
		<div class="row cta centered">
			<div class="col-md-12 col-lg-1"></div>
			<div class="col-md-12 col-lg-10">
				<h3>When the #MACTION heats up, everyone talks.</h3>
				<!--<h1>We are more than athletes:</h1>
				<p>We are students and teachers, coaches, mentors, and service leaders—committed to succeed above all else. Forged by hard work and strengthened by principles of integrity, we are prepared for every challenge we face. So it should come as no surprise that we become champions on every field and in every professional pursuit.</p>-->
			</div>
			<div class="col-md-12 col-lg-1"></div>
		</div>
	</div>
	<div id="index">
		<div class="top">
			<h2 class="rotate-3">In the News</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<a class="previous-link" href="/"><i class="fa fa-long-arrow-left"></i> Previous Page</a>
			</div>
		</div>
		<div class="row">
			<?php $the_query = new WP_Query('posts_per_page=-1'); ?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<div class="col-xs-12 col-sm-6 col-md-4 story-module">
				<?php if (get_field('story_external_link')) { echo '<a href="' . get_field('story_external_link') . '" target="_blank">'; } else { echo '<a href="' . get_permalink() . '">'; } ?>
				<?php 
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					the_post_thumbnail('home-thumb');
				} else {
					echo '<img src="'.get_bloginfo("template_url").'/images/default2.jpg" />';
				} ?></a>
				<div class="contents">
					<div class="author"><?php if (get_field('story_external_link')) { echo '<a href="' . get_field('story_external_link') . '" target="_blank">'; } else { echo '<a href="' . get_permalink() . '">'; } ?><?php the_field('story_author'); ?></a></div>
					<h4><?php if (get_field('story_external_link')) { echo '<a href="' . get_field('story_external_link') . '" target="_blank">'; } else { echo '<a href="' . get_permalink() . '">'; } ?><?php the_title(); ?></a></h4>
					<?php the_content(); ?>
				</div>
			</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
			<?php else:  ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
	<?php include 'index-footer.php'; ?>
	<?php get_footer(); ?>