	<?php get_header(); ?>
	<?php include 'index-header.php'; ?>
	<?php while(have_posts()) : the_post(); ?>
	<?php if(get_field('story_banner')) { ?>
	<?php $banner = get_field('story_banner');
		  $mainimg = $banner['sizes']['mac-story-banner']; ?>
	<div id="hero-index" class="single" style="background-image: url(<?php echo $mainimg; ?>);">
	<?php } else { ?>
	<div id="hero-index" class="single">
	<?php } ?>
		<img class="top-frame" src="<?php bloginfo('stylesheet_directory');?>/images/index-header.png" alt="header" />
		<div class="row">
			<div class="col-sm-12">
				<a href="/"><img class="logo" src="<?php bloginfo('stylesheet_directory');?>/images/header-logo.svg" alt="logo" /></a>
			</div>
		</div>
		<div class="row cta centered">
			<div class="col-md-12 col-lg-1"></div>
			<div class="col-md-12 col-lg-10">

			</div>
			<div class="col-md-12 col-lg-1"></div>
		</div>
	</div>
	<div id="story">
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<a class="previous-link" href="/pro-mac/"><i class="fa fa-long-arrow-left"></i> Previous Page</a>
				<div class="author"><?php the_field('story_author'); ?> / <span><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></span></div>
				<h4><?php the_title(); ?></h4>
				<?php the_content(); ?>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<?php endwhile; ?>
	</div>
	<div class="divider-con">
		<h2 style="font-size:30px;">More Stories</h2>
		<img class="divider4" src="<?php bloginfo('stylesheet_directory');?>/images/blue-edge-1400x111-a.png" alt="divider" />
	</div>
	<?php include 'section-related.php'; ?>
	<?php include 'index-footer.php'; ?>
	<?php get_footer(); ?>