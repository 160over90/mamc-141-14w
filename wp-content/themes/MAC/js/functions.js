var $mobilebtn = $('#nav-toggle');
var $overlaymenu = $('#overlay-menu');
var $videoinit = $('#video .watch-btn');
var $videocontents = $('#video .spotlightcon');
var $currentop = $('.current-option');
var $optioninit = $('#team-option .current-option');
var $optioninit2 = $('#talk-option .current-option');
var $listoption = $('#team-option .option-list li');
var $listoption2 = $('#talk-option .option-list li');
var $gen_slogan = 'OTAP';
var $gen_team = 'Akron';
var newcanvas;

$(window).resize(function() {
	homeWelcome();
});

$('.arrow').on('mousedown', function() {
	$("#video").velocity("scroll", { 
		container: $("#faux-container"),
		duration: 1500,
		easing: "spring"
	});
});

$('#overlay-menu ul li a').on('click', function() {
	$overlaymenu.velocity("transition.fadeOut");
	$mobilebtn.removeClass('active');
});

$mobilebtn.on('mousedown', function() {
	var $this = $(this);

	$this.toggleClass('active');

	if ($this.hasClass('active')) {
		$overlaymenu.velocity("transition.fadeIn");
	} else {
		$overlaymenu.velocity("transition.fadeOut");
	}
});

$videoinit.on('mousedown', function() {
	$videocontents.css('opacity','0');
	$('#video .vid-con').velocity("transition.fadeIn");
});

$currentop.on('click', function() {
	var $this = $(this);

	$this.siblings('.option-list').toggleClass('toggle');
});

$listoption.on('click', function() {
	var $this = $(this);
	var $useroption = $this.attr("data-imageurl");
	var $optiontext = $this.text();

	$gen_team = $this.attr("data-team");

	$('.snap-back').attr('src', $useroption);
	$optioninit.text($optiontext);
	$this.parent('.option-list').toggleClass('toggle');

	makeJawn();

	// Change the URL when generating new image
    $imgTwitter = encodeURIComponent('http://www.getsomemaction.com/?slogan=' + $gen_slogan + '&team=' + $gen_team);
    $('.twitter.popup').attr('href','http://twitter.com/intent/tweet?url=' + $imgTwitter);
    $('.facebook.popup').attr('href','http://www.facebook.com/sharer.php?u=' + $imgTwitter);
});

$listoption2.on('click', function() {
	var $this = $(this);
	var $useroption = $this.attr("data-imageurl");
	var $optiontext = $this.text();

	$gen_slogan = $this.attr("data-slogan");

	$('.snap-front').attr('src', $useroption);
	$optioninit2.text($optiontext);
	$this.parent('.option-list').toggleClass('toggle');

	makeJawn();

	// Change the URL when generating new image
    $imgTwitter = encodeURIComponent('http://www.getsomemaction.com/?slogan=' + $gen_slogan + '&team=' + $gen_team);
    $('.twitter.popup').attr('href','http://twitter.com/intent/tweet?url=' + $imgTwitter);
    $('.facebook.popup').attr('href','http://www.facebook.com/sharer.php?u=' + $imgTwitter);
});

$('#mactalk .share .download').on('click', function() {
	document.getElementById('downloader').href = newcanvas.toDataURL();
	document.getElementById('downloader').download = 'mactalk.png';
});

$('#mactalk .share .facebook').on('click', function() {
	var $sharedimage = $gen_slogan + "_" + $gen_team + ".jpg";
	var $fullpath = window.location.host + "/wp-content/themes/MAC/images/fulljpgs/" + $sharedimage;

	console.log($fullpath);
	$('meta[name="og:image"]').attr('content', $fullpath);
});

$('#mactalk .share .twitter').on('click', function() {
	var $sharedimage = $gen_slogan + "_" + $gen_team + ".jpg";
	var $fullpath = window.location.host + "/wp-content/themes/MAC/images/fulljpgs/" + $sharedimage;

	console.log($fullpath);
	$('meta[name="twitter:image"]').attr('content', $fullpath);
});

/**
 * This is the function that will take care of image extracting and
 * setting proper filename for the download.
 * IMPORTANT: Call it from within a onclick event.
*/
function downloadCanvas() {
    document.getElementById('downloader').href = newcanvas.toDataURL();
	document.getElementById('downloader').download = 'mactalk.png';
}

function makeJawn() {
	$('#thecanvas').remove();

	html2canvas(document.getElementById("your-image2"), {
		onrendered: function(canvas) {
			canvas.id = "thecanvas";
			$('body').append(canvas);
			newcanvas = document.getElementById('thecanvas');
			//$("body").append( "<p>" + canvas.toDataURL() + "</p>" );
		}
	});
}

function homeWelcome() {
	//if ($(window).width() <= 991) {
	//	$('#cta').attr('src', '/wp-content/themes/MAC/images/tape-copy.png');
	//} else {
	//	$('#cta').attr('src', '/wp-content/themes/MAC/images/home-cta.svg');
	//}
}

//POPUP CLASS
$('.popup').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
});

$(document).ready(function() {
	homeWelcome();

	$('#press-slider').slick({
		autoplay: true,
		dots: true,
		infinite: false,
		initialSlide: 1,
		prevArrow: '#slider-con .navigator.prev',
		nextArrow: '#slider-con .navigator.next'
	});

	$('.fancybox').fancybox({
		padding: 0,
		maxWidth: '1170',
		maxHeight: '820',
	  	openEffect  : 'none',
		closeEffect : 'none',
		openEffect : 'elastic',
		closeEffect : 'elastic',
		helpers : {
			overlay: {
				locked: false
			},
			media : {}
		}
	});

	if($("#homepage-flag").length > 0) {
		$('.facebook.popup').attr('href','http://www.facebook.com/sharer.php?u=http://www.getsomemaction.com/');
	} else {
		// Change the URL when generating new image
	    $imgTwitter = encodeURIComponent('http://www.getsomemaction.com/?slogan=' + $gen_slogan + '&team=' + $gen_team);
	    $('.twitter.popup').attr('href','http://twitter.com/intent/tweet?url=' + $imgTwitter);
	    $('.facebook.popup').attr('href','http://www.facebook.com/sharer.php?u=' + $imgTwitter);
	}
});