	<?php get_header(); ?>
	<?php include 'index-header2.php'; ?>
	<div id="hero-index" class="mactalk">
		<img class="top-frame" src="<?php bloginfo('stylesheet_directory');?>/images/index-header.png" alt="header" />
		<div class="row">
			<div class="col-sm-12">
				<a href="/"><img class="logo" src="<?php bloginfo('stylesheet_directory');?>/images/header-logo.svg" alt="logo" /></a>
			</div>
		</div>
	</div>
	<div id="mactalk">
		<div class="row">
			<div class="col-sm-12">
				<a class="previous-link" href="/"><i class="fa fa-long-arrow-left"></i> Previous Page</a>
			</div>
			<div class="col-md-6">
				<div id="your-image">
					<div class="overlay">
						<img class="snap-front" class="centered" src="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-01-01.png" alt="mactalk" />
					</div>
					<img class="snap-back" src="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Akron.jpg" alt="mactalk" />
				</div>
				<div class="cleaner"></div>
			</div>
			<div class="col-md-6">
				<h1>Talk Some MAC</h1>
				<ul class="instructions">
					<li>Choose Your Team</li>
					<li>Add Your MACTALK Message</li>
					<li>Share Your MACTALK With Friends, Rivals, and More.</li>
				</ul>
				<h6>Pick Your Team:</h6>
				<div id="team-option">
					<div class="current-option">University of Akron</div>
					<ul class="option-list">
						<li data-team="Akron" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Akron.jpg">University of Akron</li>
						<li data-team="BallState" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Ball_State.jpg">Ball State University</li>
						<li data-team="BowlingGreen" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Bowling_Green.jpg">Bowling Green State University</li>
						<li data-team="Buffalo" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Buffalo.jpg">University at Buffalo</li>
						<li data-team="CentralMich" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Central_Michigan.jpg">Central Michigan University</li>
						<li data-team="EasternMich" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Eastern_Michigan.jpg">Eastern Michigan University</li>
						<li data-team="WesternMich" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Western_Michigan.jpg">Western Michigan University</li>
						<li data-team="Kent" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Kent_State.jpg">Kent State University</li>
						<li data-team="Miami" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Miami_Ohio.jpg">Miami University</li>
						<li data-team="NIll" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Northern_Illinois.jpg">Northern Illinois University</li>
						<li data-team="Ohio" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Ohio.jpg">Ohio University</li>
						<li data-team="Toledo" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Toledo.jpg">University of Toledo</li>
					</ul>
				</div>

				<h6>Add Your Talk:</h6>
				<div id="talk-option">
					<div class="current-option">Our Traditions Are Passed Down Field</div>
					<ul class="option-list">
						<li data-slogan="OTAP" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-01-01.png">Our Traditions Are Passed Down Field</li>
						<li data-slogan="GG" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-02-01.png">The Grass Is Always Greener in the Next Yard</li>
						<li data-slogan="SI" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-03-01.png">Our Strong Side Is Inside</li>
						<li data-slogan="OC" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-04-01.png">Our Comebacks Happen One Return at a Time</li>
						<li data-slogan="TN" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-05-01.png">We Give Tuesdays a Name</li>
						<li data-slogan="AGB" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-06-01.png">Another Game Under the Belt</li>
						<li data-slogan="BCHF" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-04.png">Bigger the Challenge, Harder the Fight</li>
						<li data-slogan="SER" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-08.png">Streaks End, Rivalries Never Die</li>
						<li data-slogan="HG" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-09.png">When You're Hungry, You Play With Guts</li>
						<li data-slogan="HOH" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-10.png">In the Heartland, Heart's What We Got</li>
						<li data-slogan="SSC" data-imageurl="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-11.png">When We Stand, You Don't Stand a Chance</li>
					</ul>
				</div>

				<div id="your-image-mobile">
					<div class="overlay">
						<img class="snap-front" class="centered" src="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-01-01.png" alt="mactalk" />
					</div>
					<img class="snap-back" src="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Akron.jpg" alt="mactalk" />
				</div>
				<div class="cleaner"></div>

				<h6>Share:</h6>
				<ul class="share">
					<li><a class="facebook popup"><i class="fa fa-facebook"></i></a></li>
					<li><a class="twitter popup" href="http://twitter.com/intent/tweet?url=http://www.getsomemaction.com/mactalk.html"><i class="fa fa-twitter"></i></a></li>

					<li><a id="downloader" class="download" download="mactalk.png"><i class="fa fa-download"></i></a></li>
				</ul>
			</div>
		</div>
		<div id="your-image2">
			<div class="overlay">
				<img class="snap-front" class="centered" src="<?php bloginfo('stylesheet_directory');?>/images/headlines/MACtalk_Lines-01-01.png" alt="mactalk" />
			</div>
			<img class="snap-back" src="<?php bloginfo('stylesheet_directory');?>/images/backgrounds/MACtalk_Backgrounds_Akron.jpg" alt="mactalk" />
		</div>
	</div>
	<?php include 'index-footer2.php'; ?>
	<?php get_footer(); ?>