<?php
/*
 * Template Name: Toolkit
 */
?>
<?php get_header(); ?>
<?php include 'index-header.php'; ?>
<?php while(have_posts()) : the_post(); ?>
<?php if(get_field('story_banner')) { ?>
<?php $banner = get_field('story_banner');
	  $mainimg = $banner['sizes']['mac-story-banner']; ?>
<div id="hero-index" class="single toolkit" style="background-image: url(<?php echo $mainimg; ?>);">
<?php } else { ?>
<div id="hero-index" class="single toolkit">
<?php } ?>
	<img class="top-frame" src="<?php bloginfo('stylesheet_directory');?>/images/index-header.png" alt="header" />
	<div class="row">
		<div class="col-sm-12">
			<a href="/"><img class="logo" src="<?php bloginfo('stylesheet_directory');?>/images/header-logo.svg" alt="logo" /></a>
		</div>
	</div>
	<div class="row cta centered">
		<div class="col-md-12 col-lg-1"></div>
		<div class="col-md-12 col-lg-10">
			<h1><?php the_title(); ?></h1>
			<?php the_field( 'banner' ); ?>
		</div>
		<div class="col-md-12 col-lg-1"></div>
	</div>
</div>
<div id="story" class="toolkit">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<a class="previous-link" href="/"><i class="fa fa-long-arrow-left"></i> Home</a>
		</div>
		<div class="clearfix"></div>
		<?php
		if ( post_password_required() ) :
			global $post;
			$id = 'pwbox-' . ( empty( $post->ID ) ? rand() : $post->ID );
    	?>
			<div class="col-sm-4 col-sm-offset-4">
				<form action="<?php echo site_url( 'wp-login.php?action=postpass', 'login_post' ); ?>" method="POST">
					<div class="form-group">
						<label class="sr-only" for="<?php echo $id; ?>">Password</label>
						<input class="form-control" id="<?php echo $id; ?>" name="post_password" type="password" placeholder="Password" maxlength="20">
					</div>
					<div class="form-group">
						<button class="btn btn-primary form-control">Submit</button>
					</div>
				</form>
			</div>
		<?php else : ?>
			<div class="col-sm-10 col-sm-offset-1">
				<h2>Instructions</h2>
				<?php the_content(); ?>
			</div>
			<div class="col-sm-8 col-sm-offset-2">
				<h2>Downloads</h2>
				<div class="downloads-list list-group">
					<?php
					$downloads = get_field('downloads');
					foreach ( $downloads as &$download ) :
						if ( 'url' === $download['type'] ) :
							$url = $download['url'];
						elseif ( 'file' === $download['type'] ) :
							$url = $download['file'];
						endif;
					?>
						<a class="list-group-item" href="<?php echo $url; ?>" target="_blank"><?php echo $download['name']; ?></a>
					<?php
					endforeach;
					?>
				</div>
			</div>
			<div class="col-sm-1"></div>
		<?php endif; ?>
	</div>
	<?php endwhile; ?>
</div>
<?php include 'index-footer.php'; ?>
<?php get_footer(); ?>