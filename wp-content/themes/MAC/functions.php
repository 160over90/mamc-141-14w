<?php
if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 370, 290, true ); // default Post Thumbnail dimensions (cropped)

add_image_size( 'home-thumb', 470, 290, true );
add_image_size( 'mac-story-banner', 1400, 494, true );

} else {
	echo '<img src="'.get_bloginfo("template_url").'/images/default.jpg" />';
}

function new_excerpt_more($more) {
       global $post;
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more'); 

function custom_excerpt_length( $length ) {
	return 48;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

if ( ! function_exists( 'mamc_the_title' ) ) :
function mamc_the_title( $title ) {
	$patterns = array(
		'/Protected:/',
		'/Private:/',
	);

	return preg_replace( $patterns, '', $title );
}
endif;
add_filter( 'the_title', 'mamc_the_title' );