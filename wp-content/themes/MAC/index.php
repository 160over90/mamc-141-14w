	<?php get_header(); ?>
	<?php include 'index-header.php'; ?>
	<div id="hero-index" class="index">
		<img class="top-frame" src="<?php bloginfo('stylesheet_directory');?>/images/index-header.png" alt="header" />
		<div class="row">
			<div class="col-sm-12">
				<img class="logo" src="<?php bloginfo('stylesheet_directory');?>/images/header-logo.svg" alt="logo" />
			</div>
		</div>
		<div class="row cta centered">
			<div class="col-md-12 col-lg-1"></div>
			<div class="col-md-12 col-lg-10">
				<h1>We are more than athletes;</h1>
				<p>we are students and teachers, coaches, mentors, and service leaders— committed to succeed above all else. Forged by hard work and strengthened by principles of integrity, we are prepared for every challenge we face. So it should not come as a surprise that we become champions on every field and in every professional pursuit.</p>
			</div>
			<div class="col-md-12 col-lg-1"></div>
		</div>
	</div>
	<div id="index">
		<div class="top">
			<h2 class="rotate-3">Pro-MAC</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<a class="previous-link" href="/"><i class="fa fa-long-arrow-left"></i> Previous Page</a>
			</div>
		</div>
		<div class="row">
			<?php while(have_posts()) : the_post(); ?>
			<div class="col-xs-12 col-sm-6 col-md-4 story-module">
				<a href="<?php the_permalink(); ?>"><?php 
					if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						the_post_thumbnail();
					} else {
						echo '<img src="'.get_bloginfo("template_url").'/images/default.jpg" />';
					} ?></a>
				<div class="contents">
					<div class="author"><a href="<?php the_permalink(); ?>"><?php the_field('story_author'); ?> / <span><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></span></a></div>
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<?php the_content(); ?>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
	<?php include 'index-footer.php'; ?>
	<?php get_footer(); ?>