	<div id="index" class="related">
		<div class="row">
			<?php $the_query = new WP_Query('posts_per_page=3'); ?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<div class="col-xs-12 col-sm-6 col-md-4 story-module">
				<a href="<?php the_permalink(); ?>"><?php 
					if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						the_post_thumbnail();
					} else {
						echo '<img src="'.get_bloginfo("template_url").'/images/default.jpg" />';
					} ?></a>
				<div class="contents">
					<div class="author"><a href="<?php the_permalink(); ?>"><?php the_field('story_author'); ?> / <span><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></span></a></div>
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<?php the_excerpt(); ?>
				</div>
			</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
			<?php else:  ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>