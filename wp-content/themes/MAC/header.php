<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" href="http://www.getsomemaction.com/wp-content/themes/MAC/images/favicon.ico" type="image/x-icon"/>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@MACSports">
<?php
  if (isset($_REQUEST['team']) && isset($_REQUEST['slogan'])) {
    $PHPteam = $_REQUEST['team'];
    $PHPslogan = $_REQUEST['slogan'];
    
    // Set the OG URL
    echo '<meta property="twitter:url" content="http://www.getsomemaction.com/?slogan='.$PHPslogan.'&team='.$PHPteam.'">';
    echo '<meta property="og:url" content="http://www.getsomemaction.com/?slogan='.$PHPslogan.'&team='.$PHPteam.'">';

    // Set OG image
    echo '<meta property="twitter:image" content="http://www.getsomemaction.com/wp-content/themes/MAC/images/fulljpgs/'.$PHPslogan.'_'.$PHPteam.'.jpg">';
    echo '<meta property="og:image" content="http://www.getsomemaction.com/wp-content/themes/MAC/images/fulljpgs/'.$PHPslogan.'_'.$PHPteam.'.jpg">';

    echo '<meta name="twitter:title" content="MACTALK">';
    echo '<meta name="twitter:description" content="Want a piece of the #MACTION? Create your own at GetSomeMACtion.com">';

  } else {
    
    // Set default URL
  	echo '<meta property="og:url" content="http://www.getsomemaction.com/">';
  	echo '<meta property="twitter:url" content="http://www.getsomemaction.com/">'; 
	 // Set default image
    echo '<meta property="og:image" content="http://www.getsomemaction.com/wp-content/themes/MAC/images/default.jpg">';
    echo '<meta property="twitter:image" content="http://www.getsomemaction.com/wp-content/themes/MAC/images/default.jpg">';

    echo '<meta name="twitter:title" content="#MACTION Speaks Louder Than Words">';
    echo '<meta name="twitter:description" content="Under the spotlight we show our true colors.">';
  }
 ?>

<title>Mid-American Conference</title>
<?php wp_head(); ?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7709574/706408/css/fonts.css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory');?>/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory');?>/slick/slick-theme.css"/>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" type="text/css" media="screen" />
<script src="<?php bloginfo('stylesheet_directory');?>/js/modernizr.js"></script>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-65777660-1', 'auto');
 ga('send', 'pageview');

</script>
</head>

<body style="height:100%;">