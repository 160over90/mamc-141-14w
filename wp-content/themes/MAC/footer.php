	<?php wp_footer(); ?>
	<script src="<?php bloginfo('stylesheet_directory');?>/js/jquery-2.1.3.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/js/velocity.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/js/velocity.ui.js"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/js/html2canvas.js"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/fancybox/source/jquery.fancybox-media.js?v=2.1.5"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/slick/slick.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory');?>/js/functions.js"></script>
</body>
</html>