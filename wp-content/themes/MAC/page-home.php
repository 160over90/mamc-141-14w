	<?php get_header(); ?>
	<?php include 'home-header.php'; ?>
	<div id="hero">
		<div class="row">
			<div class="col-sm-12">
				<img class="logo" src="<?php bloginfo('stylesheet_directory');?>/images/header-logo.svg" alt="logo" />
			</div>
		</div>
		<div id="welcome-con" class="row">
			<div class="col-md-6">
				<img id="welcome" src="<?php bloginfo('stylesheet_directory');?>/images/home-welcome.svg" alt="welcome" />
			</div>
			<div class="col-md-6">
				<img id="cta" src="<?php bloginfo('stylesheet_directory');?>/images/tape-copy.png" alt="cta" />
			</div>
		</div>
		<div class="bottom">
			<a><img class="arrow" src="<?php bloginfo('stylesheet_directory');?>/images/home-heroarrow.png" alt="arrow" /></a>
		</div>
	</div>
	<div class="divider-con">
		<h2>Video</h2>
		<img class="divider1" src="<?php bloginfo('stylesheet_directory');?>/images/divider-home1.svg" alt="divider" />
	</div>
	<div id="video">
		<div class="overlay">
			<div class="vid-con centered">
				<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/WmbTOppAWjU' frameborder='0' allowfullscreen></iframe></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">

			</div>
			<div class="col-md-8">
				<div class="spotlightcon">
					<div class="overlay">
						<div class="centered">
							<img class="spotlight" src="<?php bloginfo('stylesheet_directory');?>/images/home-welcome2.svg" alt="spotlight" />
							<div class="watch-btn"><i class="fa fa-play-circle"></i>Watch Now</div>
						</div>
					</div>
					<img class="teamlogos" src="<?php bloginfo('stylesheet_directory');?>/images/team-logos.png" alt="teamlogos" />
				</div>
			</div>
			<div class="col-md-2">

			</div>
			<img class="spotlight-img left" src="<?php bloginfo('stylesheet_directory');?>/images/spotlight-left.png" />
			<img class="spotlight-img right" src="<?php bloginfo('stylesheet_directory');?>/images/spotlight-right.png" />
		</div>
	</div>
	<?php if( have_rows('macdigital_archive') ) { ?>
	<div class="divider-con">
		<h2 class="macdigital">Off the Turf</h2>
	</div>
	<div id="MACDigital">
		<div class="row">
			<?php while ( have_rows('macdigital_archive') ) : the_row(); ?>
			<?php $video = get_sub_field('macdigital_video');

			// use preg_match to find iframe src
			preg_match('/src="(.+?)"/', $video, $matches_url );
			$src = $matches_url[1];

			preg_match('/embed(.*?)?feature/', $src, $matches_id );
			$id = $matches_id[1];
			$id = str_replace( str_split( '?/' ), '', $id );

			$thumb = 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg';
			?>
			<div class="col-sm-4">
				<a class="thumb fancybox" href="<?php echo $src; ?>">
					<div class="overlay">
						<i class="fa fa-play-circle centered"></i>
					</div>
					<img src="<?php echo $thumb; ?>" alt="thumb" />
				</a>
				<h4><a class="fancybox" href="<?php echo $src; ?>"><?php echo get_sub_field('macdigital_title'); ?></a></h4>
			</div>
			<?php endwhile; ?>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<a href="https://www.youtube.com/user/MACDigitalNetwork/featured" target="_blank" class="line-btn">Watch More Videos <i class="fa fa-external-link"></i></a>
			</div>
		</div>
	</div>
	<?php } ?>
	<div id="share-divider">
		<h5>Share the #Maction</h5>
		<ul>
			<li><a class="facebook popup"><i class="fa fa-facebook"></i></a></li>
			<li><a class="twitter popup" href="http://twitter.com/intent/tweet?url=http://www.getsomemaction.com/&text=%23MACTION+speaks+louder+than+words:"><i class="fa fa-twitter"></i></a></li>
			<li><a href="mailto:?subject=Get some MACtion!&body=#MACTION speaks louder than words: http://www.getsomemaction.com"><i class="fa fa-envelope"></i></a></li>
		</ul>
		<span id="homepage-flag" style="display: none" />
		<div class="bottom">
			<h2>Pro-MAC</h2>
		</div>
	</div>
	<div class="divider-con divider5">
		<h2 class="rotate-357 in-the-news">In the News</h2>
	</div>
	<div id="home-stories">
		<div class="row">
			<div class="col-md-2">

			</div>
			<div class="col-md-8">
				<h3>When the <span>#MACTION</span> heats up, everyone talks.</h3>
			</div>
			<div class="col-md-2">

			</div>
		</div>
		<?php $the_query = new WP_Query('posts_per_page=6&orderby=rand'); ?>
		<?php if ( $the_query->have_posts() ) : ?>
		<div id="slider-con" class="row">
			<div class="navigator prev"><i class="fa fa-chevron-left"></i></div>
			<div class="navigator next"><i class="fa fa-chevron-right"></i></div>
			<div class="col-sm-12">
				<div id="press-slider">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="slide">
						<div class="story-module">	
							<?php if (get_field('story_external_link')) { echo '<a href="' . get_field('story_external_link') . '" target="_blank">'; } else { echo '<a href="' . get_permalink() . '">'; } ?>
							<?php 
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								the_post_thumbnail('home-thumb');
							} else {
								echo '<img src="'.get_bloginfo("template_url").'/images/default2.jpg" />';
							} ?></a>
							<div class="contents">
								<div class="author"><?php if (get_field('story_external_link')) { echo '<a href="' . get_field('story_external_link') . '" target="_blank">'; } else { echo '<a href="' . get_permalink() . '">'; } ?><?php the_field('story_author'); ?></a></div>
								<h4><?php if (get_field('story_external_link')) { echo '<a href="' . get_field('story_external_link') . '" target="_blank">'; } else { echo '<a href="' . get_permalink() . '">'; } ?><?php the_title(); ?></a></h4>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<a class="green-btn" href="/in-the-news">More News <i class="fa fa-angle-double-right"></i></a>
			</div>
		</div>
		<?php else:  ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
	</div>
	<div class="divider-con">
		<h2 class="rotate-357">Mactalk</h2>
		<img class="divider2" src="<?php bloginfo('stylesheet_directory');?>/images/green-edge-1400x193.png" alt="divider" />
	</div>
	<div id="home-mac-talk">
		<div class="row">
			<div class="col-md-6">
				<img class="fight" src="<?php bloginfo('stylesheet_directory');?>/images/home-welcome3.svg" alt="fight" />
			</div>
			<div class="col-md-6">
				<p>Our strongest statements are made on the field and on the court, but this won't stop us from a little #MACTALK outside the lines.  Because the only thing as strong as our character is our pride.</p>
				<a class="green-btn" href="/mactalk/">MACTALK Generator <i class="fa fa-angle-double-right"></i></a>
				<p class="invite"><i class="fa fa-star"></i> Create your own MACTALK meme now <i class="fa fa-star"></i></p>
			</div>
		</div>
	</div>
	<div class="divider-con">
		<h2>#Maction</h2>
		<img class="divider3" src="<?php bloginfo('stylesheet_directory');?>/images/blue-edge-1400x111-a.png" alt="divider" />
	</div>
	<div id="home-social-stadium">
		<div class="row">
			<div class="col-sm-6">
				<h3>Wherever we Go,<br />The <span>#MACTION</span> Follows</h3>
				<p class="description">Keep your finger on the pulse of the<br />Mid-American Conference and all of the<br />#MACTION–every day, every game, every play.</p>
				<p class="description mobile">Keep your finger on the pulse of the Mid-American Conference and all of the #MACTION–every day, every game, every play.</p>
				<a class="green-btn" href="http://mac.sidearmsocial.com/social/" target="_blank">Social Stadium <i class="fa fa-angle-double-right"></i></a>
			</div>
			<div class="col-sm-6">

			</div>
			<img class="product-placement" src="<?php bloginfo('stylesheet_directory');?>/images/home-socialstadium.png" alt="socialstadium" />
		</div>
	</div>
	<?php include 'home-footer.php'; ?>
	<?php get_footer(); ?>