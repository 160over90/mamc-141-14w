<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mac_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=L-VAjhTA%BUzT0Mx|T%_uPQ_=gFG;g9u%;~o(hW:{G/| TnYDVS-1ItA__;]Aly');
define('SECURE_AUTH_KEY',  '-!rz{|*wmJ(4^,g.@R!/.1gz<N+1f>0> iu-~b5>Dl4>h@j&Slfq)|->c}@O]/m5');
define('LOGGED_IN_KEY',    '+cXfFY|Xx].|!bi0e#jzZ.zZ+Q(2p-rnf;t#/BGQW14,Ux?V{OOD$e_:1/8i{^:%');
define('NONCE_KEY',        's#+v/YIIO^iUzs{cM@;X4b3{ER|DT(u`,9R{OqpTkWzn-wfZDWOkz+WS6V<NM0-~');
define('AUTH_SALT',        'yV./(^VAS,PTC[zQ&=f UIe^t-@d^{eT9qU||-Q:3{O#H!^,q-zaV2d}Lnw*7TDQ');
define('SECURE_AUTH_SALT', '@@Z6W*oyR>6M;+Lgm0|U)&r_n2`}yykZ6ZU+%aR2|lO19T|ad6Z}H>gV/g(q^=]a');
define('LOGGED_IN_SALT',   'xgA^u;$dg=eCTGv18_g1*2 +$da;gC!y-gMlhUxq),MeQ8.zX-hFfj-0EE)P?k<7');
define('NONCE_SALT',       '/[(WR`b|ZqCihck*(/lq(XgB&iC-@x%s2*&RFz,48Cos!n7=Y[e +K|Gl,!Opm0>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
